%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Nicholas Dunn CV/Resume -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Intended LaTeX compiler: xelatex
\documentclass[12pt,a4paper]{awesome-cv}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- AwesomeCV Black Magic -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\acvHeaderSocialSep}{\enskip\cdotp\enskip}
\renewcommand{\acvHeaderIconSep}{~}
\renewcommand*{\bodyfontlight}{\sourcesanspro}
\renewcommand*{\entrylocationstyle}[1]{{\fontsize{10pt}{1em}\bodyfontlight\slshape\color{awesome} #1}}
\renewcommand*{\subsectionstyle}{\entrytitlestyle}
\renewcommand*{\headerquotestyle}[1]{{\fontsize{8pt}{1em}\bodyfont #1}}
\setcounter{secnumdepth}{1}
\fontdir[fonts/]
\colorlet{awesome}{awesome-skyblue}
\setbool{acvSectionColorHighlight}{true}
\colorizelinks[awesome-red]
\name{Nicholas}{Dunn}
\position{Senior DevOps/DevSecOps Engineer · Platform Engineer · Project/Technical Lead}
\photo[circle,noedge,right]{images/2024-03-18.jpeg}
\email{nickdunn@carrierpigeon.email}
\github{ndunn990}
\gitlab{ndunn}
\linkedin{nd137}
\hypersetup{
	pdfauthor={Nicholas Dunn},
	pdftitle={Senior DevOps/DevSecOps Engineer · Platform Engineer · Technical Lead},
	pdfkeywords={cv,resume},
	pdfsubject={},
	pdflang={English}}
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Header/Footer Configuration -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\makecvheader
\makecvfooter{\today\\~}{Nicholas Dunn\\\textup{\tiny Source at https://gitlab.com/ndunn/cv\_resume}}{\thepage\\~}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Introduction -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cvsection{Introduction}

\begin{cvparagraph}
	I am currently located near Montpelier, Vermont. I am an experienced and motivated Platform and DevOps/DevSecOps Engineer, as well as a \emph{former} Captain with the Army Signal Corps in the Ohio Army National Guard. I'm a proven team leader who thrives on excellence and proficiency, and an IT professional with a passion to learn, tinker, build, and solve problems. While I prefer remote opportunities, I'm happy to consider positions that involve occasional travel or positions that have hybrid agreements, depending on the location(s).
\end{cvparagraph}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Overview of My Experience -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cvsection{Overview of My Experience}

\begin{cvparagraph}
	\emph{Bottom line up front.}
\end{cvparagraph}

\begin{cventries}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- General -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cvsubentry{General}{}
	{\begin{cvitems}
			\item I have extensive experience building and maintaining pipelines in various CI/CD tools. This includes (but is not limited to) Octopus Deploy, Team Foundation Server, Azure DevOps, Github, Gitlab, Jenkins, ArgoCD, and FluxCD. I have experience administrating self-hosted CI/CD tools and I have experience migrating organizations to new CI/CD systems.
			\item I have experience building/deploying, maintaining, and supporting Kubernetes clusters in Azure, AWS, and on-premises using virtual machines and bare metal in both the private and public sector.
				{\begin{itemize}
						\item My experience with Kubernetes includes Helm, Kustomize, etc.
						\item I have been fortunate enough to enjoy some experience with more esoteric tools focused around Kubernetes. I've had experience with hyper-converged infrastructure and non-cloud-hosted Kubernetes storage solutions (Longhorn, Rook/Ceph).
					\end{itemize}}
			\item I have deployed, maintained, and automated a service mesh (Consul and Istio, primarily). This has largely been in environments that utilize Kubernetes, but I have also implemented a service mesh in a more mixed environment that included Kubernetes clusters and more traditional application stacks running on VMs.
			\item I have designed, built, maintained, and automated large environments in Azure and AWS. I tend to view cloud providers much like languages, so I welcome any chance to become proficient with another.
			\item I've never held a 'Platform Engineer' title, but platform engineering and infrastructure-as-code (IaC) has been a significant focus of mine throughout much of my career since 2015. This includes configuration management (Chef, Ansible) and provisioning tools (Terraform, Ansible, Pulumi). I have experience leading the design efforts for an organization's overall IaC initiative.
			\item In recent years, I have some experience supporting platforms designed for AI/ML workloads largely focused on utilizing Kubernetes.
			\item I have deployed, maintained, and supported application monitoring systems. This has largely involved Prometheus/Grafana, standard cloud provider tools, log aggregation tools using ElasticSearch.
			\item I have deployed, maintained, and automated ElasticSearch, RabbitMQ, and Kafka clusters in both traditional deployments and on Kubernetes for several organizations. I have done this both on-premises and in the cloud.
			\item I have experience deploying, maintaining, and automating various data systems. This includes (but is not limited to) Postgres, Microsoft SQL, Data Bricks, and InfluxDB. I've also had the chance to utilize cloud-managed options, such as AWS RDS and Azure Managed SQL.
			\item I have experience deploying, maintaining, and automating secret management tools. This includes HashiCorp's Vault.
			\item I have experience applying DevOps principles to Cybersecurity (and vice versa), sometimes referred to as DevSecOps.
			\item I have experience supporting AI/ML workloads, sometimes referred to as MLOps.
		\end{cvitems}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- Languages -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cvsubentry{Languages}{}
	{\begin{cvitems}
			\item I'm comfortable with Golang, PowerShell, Bash, Ruby, and Python. I've used them all extensively for automation, scripting, and more.
			\item I've provided support in a DevOps role for the following languages: .NET, Java, Java Script, and a long list of front-end web languages.
			\item I consider myself highly proficient with Terraform and Ansible. I'm also familiar with Pulumi, and to a lesser extent, Crossplane.
		\end{cvitems}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- Other -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cvsubentry{Other}{}
	{\begin{cvitems}
			\item If necessary for a client or contract, I currently hold a Top Secret (TS) clearance.
		\end{cvitems}}
\end{cventries}
\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Experience Breakdown -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cvsection{Civilian \& Military Careers}

\begin{cvparagraph}
	There is an overlap in some of my experience between my civilian and military careers. As a member of the Army National Guard, I was part-time until activated according to need. Typically, this meant two to six days per month and a two to three consecutive week period per year for training. In addition, there were times we were activated for disaster relief, various operations to assist local communities when necessary, and deployments overseas. The unique challenge for most reserve elements in the U.S. military is that you are expected to meet the same technical, professional, and physical standards as those who serve full-time. As a result, I enjoyed two full careers simultaneously for a time - one military and one civilian.
\end{cvparagraph}

\begin{cvparagraph}
	Some of the positions might seem strange to the uninitiated and there are some overlaps in positions due to personnel shortages. Essentially, I specialized as a technical expert in communications, which includes everything from simple FM/HF radio communications to sophisticated digital communications (satelite communications, rapidly deployed datacenters in-the-field and on-the-go, networking systems, cyber security, etc.). As a technical expert, my position as a commissioned officer typically leaned more towards architecting and planning; providing recommendations and analyses to leadership; and standard management duties for a small (six to ten personnel) team. I've included my best attempt at a civilian comparison for each military role.
\end{cvparagraph}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Civilian Experience -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cvsection{Civilian Experience}

\begin{cventries}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- Raft -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cventry{Raft}{Reston, VA (Remote)}{Senior DevSecOps Engineer}{July 2023 -- Present}
	{{\begin{cvitems}
					\item Maintain fully operational systems while also laying the foundation for future product enhancements.
					\item  Build, maintain, and support Gitlab CI/CD. Build, maintain, and support Github Actions.
					\item  Build, develop, and maintain container images and Helm charts in accordance with government security requirements.
					\item  Build, maintain, and support all application environments.
					\item  Build, maintain, and support variety of Kubernetes environments (Azure, AWS, and Bare metal) in order to provide a platform for AI/ML initiatives (MLOPS).
					\item  Improve and iterate improvements to the platform.
					\item  Build, maintain, and support data pipelines in order to support any and all AI/ML initiatives on the platform.
					\item  Build, improve, document, manage, and maintain the organization’s application performance management (APM), application monitoring, and infrastructure monitoring to ensure the health of existing production systems.
					\item  Coordinate and execute upgrades for support tools/systems/applications/infrastructure to ensure customer's utilizing the platform are minimally affected.
					\item  Analysis and integration of third party tools where necessary and participate in technology and tools evaluations or 'proof-of-concepts.'
					\item  Develop, design, and maintain Infrastructure-as-code using Ansible and Terraform, as well as develop and support standards for further development.
					\item  Architect and maintain scalable infrastructure automation for a wide-variety of use-cases in both cloud (Azure/AWS) and bare metal environments.
					\item  Design, build, and maintain methods by which to deploy custom data ingestion and management platforms in a variety of environments.
					\item  Build, maintain, and automate hyper-converged infrastructure (primarily Harvester due to RKE2 requirements) for rapid development.
					\item  Architect the organization's Infrastructure-as-Code (IAC) for all required cloud providers and bare metal environments, primarily Terraform and Ansible.
					\item  Work with engineering teams to 'on-board' new customers in order to determine their needs and determine their unique environment limitations. If necessary, adapt our processes and automation to meet those needs and operate within those limitations.
				\end{cvitems}}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- Strateos -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cventry{Strateos}{Menlo Park, CA (Remote)}{Senior DevOps Engineer}{May 2022 -- July 2023}
	{{\begin{cvitems}
					\item Maintain fully operational systems (AWS/on-premises) while also laying the foundation for future product enhancements.
					\item Build, maintain, and support CI/CD systems. Represent release management policies and processes to ensure development and infrastructure teams are following established guidelines.
					\item Build, maintain, and support all application environments.
					\item Build, improve, document, manage, and maintain the organization’s application performance management (APM), application monitoring, and infrastructure monitoring to ensure the health of existing production systems.
					\item Responsible for coordinating and executing upgrades for support tools/systems/applications/infrastructure to ensure Strateos applications in both production and development environments are minimally affected.
					\item Represent CI/CD, application service orchestration, and automation policies and processes to ensure all teams are following established guidelines.
					\item Troubleshoot compilation and build failures to support software engineers.
					\item Responsible for the reduction of technical debt and the optimization of cloud costs where possible.
					\item Build, maintain, and support application service orchestration systems (primarily Kubernetes).
					\item Develop, design, and maintain Infrastructure-as-code initiatives, as well as develop and support standards for further development.
					\item Manage, develop, and automate the build and deployment processes for both software and infrastructure.
					\item Architect and maintain scalable infrastructure automation.
					\item Provide demonstrations or training on current processes, tools, and methods of automation used for development operations and system administration.
				\end{cvitems}}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- IGS Energy -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cventry{IGS Energy}{Columbus, OH}{Senior DevOps Engineer}{May 2016 -- May 2022}
	{{\begin{cvitems}
					\item Manage, develop, and automate the build and deployment processes for both software and infrastructure.
					\item Manage, develop, automate, and improve our vision for Infrastructure as Code (IaC).
					\item Manage, maintain, and improve our Azure DevOps (formerly known as Visual Studio Team Services) instances.
					\item Troubleshoot compilation and build failures to support developers.
					\item Carefully manage software releases in all environments, of which there are currently eight including production, and be prepared to quickly create new environments when necessary.
					\item Develop, improve, document, manage, and maintain the organization’s application performance management (APM), application monitoring, and infrastructure monitoring to ensure the health of existing production systems.
					\item Coordinate software upgrades and other support tools to ensure applications in production and the software development process are minimally affected.
					\item Represent release management policies and processes to ensure development and infrastructure teams are following established guidelines.
					\item Work closely with the infrastructure team to assist where necessary, while expected to be the ‘lead system administrator’ for all application and web servers.
					\item Work closely with development team leads to solve build system issues and develop build standards, and to ensure best practices are followed. Furthermore, document any potential for improvement and implement it if deemed valuable.
					\item Responsible for the reduction of ‘technical debt’ within the enterprise architecture.
					\item Improve the efficiency and scalability of the continuous integration environment via automation.
					\item Responsible for the analysis of third party technology and tools evaluations, or ‘proof-of-concepts,’ as well as their eventual integration if adopted.
					\item Create and maintain all continuous integration process documentation.
					\item Provide demonstrations or training on current processes, tools, and methods of automation used for development operations and system administration.
				\end{cvitems}}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- ComResource -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cventry{ComResource, Inc.}{Columbus, OH}{DevOps Engineer}{Aug 2015 -- May 2016}
	{{\begin{cvitems}
					\item I was employed as an IT Consultant mostly focused on System Administration and Development Operations. During my time with ComResource, I primarily acted as a consultant for their client, IGS Energy. There was an expectation that IGS Energy would hire me full-time if the fit was right for both the company and myself. My responsibilities in this position were similar to those of my current position with IGS Energy.
				\end{cvitems}}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- DISA -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cventry{Defense Information Systems Agency (DISA)}{Columbus, OH}{}{Jun 2014 -- Aug 2015}{}

	% Cyber Defenses Contract
	\cvsubentry{System and Network Administrator with Cyber Defenses, Inc.}{Dec 2014 -- Aug 2015}
	{\begin{cvitems}
			\item Create user accounts (Active Directory / Exchange mailboxes) on Windows 2003/2008.
			\item Manage over 2,500 individual and group Active Directory and Microsoft Exchange email accounts, including organization unit (OU) and domain assignment.
			\item Regularly screen incoming emails in IronPort for prohibited content and work closely with IT Security to provide regular monitoring to identify any possible security violations.
			\item Monitor local system performance and resources and make changes necessary for sustained system performance.
			\item Monitor the remote monitoring and management system and server alerts and notifications, and then respond accordingly to mitigate any downtime for services provided by those systems.
			\item Configure, maintain, and monitor Blue Coat proxies for intranet filtering on the domain networks.
			\item Monitor network traffic using Cisco Ironport and Blue Coat Reporter.
			\item Resolve any network related issues using remote management of virtual and physical servers, while providing touch labor on customer servers, switches, and other network equipment to include troubleshooting and console interface.
			\item Identify and troubleshoot any Tier 1 issues with communication security (COMSEC) devices (TACLANE, KIV-7, etc.) used by the network.
			\item Assist customers with proxy issues in the continental United States or overseas accessing web services through the proxy, utilizing email services through the proxy, or utilizing our project’s exchange services.
			\item Routinely uses ticketing management system to maintain a record of all customer issues and the details therein.
			\item Maintain and update DoD Coalition Support web pages on the domain using HTML as needed.
			\item Develop, update, maintain, and utilize Powershell scripts for various administrative duties.
			\item Install and update various services on the servers, such as updates to Cisco IronPort, changes in COMSEC, or anti-virus.
			\item Check server system logs to identify incidents or trends that may indicate or diagnose the cause of an issue.
			\item Create and update standard operating procedure (SOP) documentation.
		\end{cvitems}}

	% TechGuard Contract
	\cvsubentry{IT Specialist with TechGuard Security}{Jun 2014 -- Dec 2014}
	{\begin{cvitems}
			\item Serve as a technical analyst and primary customer liaison with responsibility for resolving difficult customer problems within a wide variety of applications, and desktop configurations.
			\item Refer problems requiring highly specialized expertise to the senior technical analyst.
			\item Provide technical advice and assistance to include troubleshooting, diagnosing and resolving customer application problems.
			\item Review, validate and standardize problem resolutions for inclusion in the problem resolution database.
			\item Research, evaluate, and provide feedback on problematic trends and patterns in customer support requirements for new or modified systems and services based upon an analysis of business needs and practices.
			\item Apply my knowledge of and skill in applying support concepts, methods and procedures for troubleshooting and resolving the most complex customer problems.
		\end{cvitems}}
\end{cventries}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Military Experience -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% There should be no further updates to this section

\cvsection{Military Experience}

\begin{cventries}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% ---- OHARNG -----
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\cventry{The Ohio Army National Guard; U.S. Army}{Mostly Ohio}{Commissioned Officer}{May 2012 -- Oct 2020}{}

	% Company Commander
	\cvsubentry{Company Commander}{Oct 2017 -- Oct 2020}
	{\begin{cvitems}
			\item In the civilian world, this would be considered strictly a management position with roughly one hundred or more personnel. I was in a unit that hanlded logistical operations for units that trained to respond to nuclear, biological, or chemical incidents.
			\item Command the Headquarters and Headquarters Detachment for a brigade headquarters unit in the Ohio Army National Guard.
			\item Acts as the Officer in Charge of the Incident Base support element for the FEMA Region V Homeland Response Force.
			\item Responsible for ensuring all unit administrative and training functions are being met.
			\item Ensure high levels of unit preparedness by tracking, managing and maintaining personnel and equipment readiness.
			\item Support and prepare unit for no-notice domestic operations within the continental United States.
			\item Maintain and account for over \$34 million in Army property and equipment.
			\item Responsible for the health, welfare and readiness of over 80 Soldiers, NCOs and officers.
		\end{cvitems}}

	% Battalion S6
	\cvsubentry{Battalion S6 (Communications Officer)}{Sep 2015 -- Oct 2017}
	{\begin{cvitems}
			\item In the civilian world, this would probably be something akin to an IT manager or director in a small company. I had a team of roughly six to eight and we were in charge of all communication requirements, training, and planning for a battalion-sized element (in this case, roughly 500 personnel).
			\item Advise the commander and staff on all communications related issues.
			\item Develop policies and procedures for the battalion regarding communications. Responsible for Electronic Counter-Counter Measures (ECCM).
			\item Establish and maintain networks within the battalion.
			\item Work closely with the S3 (Operations Staff Officer) and the Executive Officer, especially during future planning.
			\item Maintain and account for all communications equipment, which includes (but is not limited to): FM radio equipment, VHF/UHF equipment, communications security equipment, computer network equipment (switches, routers, etc.), secure workstations (i.e. imaged laptops), servers, digital storage (hard drives), etc.
		\end{cvitems}}

	% XO
	\cvsubentry{Company Executive Officer}{Sep 2012 -- Sep 2015}
	{\begin{cvitems}
			\item In the civilian world, I would probably consider this similar to a position in middle-management. This unit was a Communications Company (roughly 100 personnel), which one could call an IT consulting firm that can be out-sorced to other organizations.
			\item Second in command in case of the Company Commander’s absence.
			\item Responsible for the unit’s “additional duties,” to ensure the unit meets Army requirements.
			\item Ensure all necessary reports are delivered to those above company leadership, namely Headquarters and Headquarters Company (HHC) as well as to Battalion leadership.
			\item Keep the commander apprised of all company equipment, including the maintenance plan for any broken-down equipment.
			\item Responsible for all company logistical needs to maintain unit readiness and to accomplish the mission.
			\item Assist the commander with any company-level planning for the entire training year.
			\item Directly responsible for the company’s administration requirements having to do with all personnel.
			\item Ensure unit readiness in regards to training, equipment, and personnel.
			\item Ensure the commander’s intent for any given task is communicated and clearly understood by all subordinates, especially the company’s Platoon Leaders
		\end{cvitems}}

	% PL
	\cvsubentry{Signal Platoon Leader}{May 2012 -- Nov 2013}
	{\begin{cvitems}
			\item In the civilian world, this position is something like a 'lead' position. I was in charge of a Communications Platoon (roughly 30 personnel), which is almost like a small IT/communications team that can be out-sourced to others depending on need.
			\item Plan and establish all communication to support the mission tasked to my platoon.
			\item Plan, establish, and maintain power generation for the unit, including a proper plan to ground all equipment and a proper power load plan to avoid damage to equipment and personnel.
			\item Plan and establish basic telephone or VoIP networks (including call managers, switches, caller ID, etc.)
			\item Plan and establish local area networks anywhere necessary using TDMA/FDMA via satellite or High Capacity Line of Sight (HCLOS) to communicate with a DoD “ISP” and provide a reliable and secure internet connection.
			\item Plan and establish radio networks using FM, HF, AM, etc. while maintaining security integrity with frequency hop and encryption
			\item Manage the maintenance and daily operation of local area networks
			\begin{itemize}
				\item Establish and maintain a helpdesk to troubleshoot any issues 24/7 for all units and personnel using the network.
			\end{itemize}
			\item Manage the maintenance and daily operation of all signal equipment, including Blue Force Tracker (BFT), Satellite Transportable Terminals (STTs), High Frequency radios, Single Channel Ground and Airborne Radio Systems (SINCGARS), HF radio systems, Multiband Inter/Intra Team Radio systems (MBITRs), Joint Network Nodes/Command Post Nodes (JNN/CPN), routers, switches, personal computers modified for DoD use, etc.
			\item Directly responsible for all training to maintain my platoon’s effectiveness in its mission and in combat.
			\item Provide technical expertise to advise my commander of communication capabilities and limitations within the context of a provided mission.
		\end{cvitems}}
\end{cventries}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ---- Education -----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\cvsection{Education \& Certifications}

\begin{cventries}
	\cventry{Security+}{}{CompTIA}{September 2023 -- September 2026}{}

	\cventry{Bachelor's Degree in History}{Greenville, SC}{Furman University}{Aug 2008 -- May 2012}{}

	\cventry{Bachelor's Degree in Philosophy}{Greenville, SC}{Furman University}{Aug 2008 -- May 2012}{}

	\cventry{U.S. Army Reserve Officer Training Corps (ROTC)}{Greenville, SC}{Furman University}{Aug 2008 -- May 2012}{}

	\cventry{U.S. Army Signal Corps Basic Officer Leadership Course (SBOLC)}{Augusta, GA}{Fort Gordon}{Aug 2012 -- Feb 2013}{}
\end{cventries}
\end{document}
